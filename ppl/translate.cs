﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Forms;

namespace ppl
{
    class translate
    {
        data datos = new data();

        public string getWord(string palabra)
        {
            string pl = "'" + palabra + "'";
            string fo = datos.getSingleData("words","es="+pl);
            if (fo == null)
            {
                fo = palabra;
            }
            return fo;
        }

        public string UppercaseFirst(string s)
        {
            if (string.IsNullOrEmpty(s))
            {
                return string.Empty;
            }
            char[] a = s.ToCharArray();
            a[0] = char.ToUpper(a[0]);
            return new string(a);
        }



    }
}
