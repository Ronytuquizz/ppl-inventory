﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ppl
{
    public partial class setting : Form
    {
        public setting()
        {
            InitializeComponent();
        }

        private void tabControl1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void tabPage1_Click(object sender, EventArgs e)
        {

        }

        private void button3_Click(object sender, EventArgs e)
        {
            try
            {
                string lan = lang.Text;
                if (lan=="ENGLISH")
                {
                    lan = "en";
                }
                if (lan == "ESPAÑOL")
                {
                    lan = "es";
                }
                ppl.Properties.Settings.Default.language = lan;
                ppl.Properties.Settings.Default.companyName = companyname.Text;
                ppl.Properties.Settings.Default.DBpath = txtDBpath.Text;
                ppl.Properties.Settings.Default.Save();
                MessageBox.Show("Guardado");
                this.Hide();
            }
            catch (Exception)
            {

                throw;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void setting_Load(object sender, EventArgs e)
        {
            getSetting();
            traducir();
        }

        public void traducir()
        {
            string lan = ppl.Properties.Settings.Default.language.ToLower();
            if (lan == "en")
            {
                translate tran = new translate();
                //label2.Text = tran.getWord(label2.Text.ToLower());
                foreach (Control item in this.Controls)
                {
                    string translated = tran.getWord(item.Text.ToLower());
                    item.Text = tran.UppercaseFirst(translated);
                    if (item is GroupBox)
                    {
                        foreach (Control GroupBoxItem in item.Controls)
                        {
                            if (!(GroupBoxItem is PictureBox) && !(GroupBoxItem is ComboBox) && !(GroupBoxItem is TextBox))
                            {
                                string GroupBoxTranslatedItem = tran.getWord(GroupBoxItem.Text);
                                GroupBoxItem.Text = tran.UppercaseFirst(GroupBoxTranslatedItem);
                            }

                        }
                    }

                    

                }
            }
        }

        public void getSetting()
        {
            string lan = ppl.Properties.Settings.Default.language;
            string DBpath = ppl.Properties.Settings.Default.DBpath; 
            if (lan == "en")
            {
                lan = "ENGLISH";
            }
            if (lan == "es")
            {
                lan = "ESPAÑOL";
            }
            lang.Text = lan;
            companyname.Text = ppl.Properties.Settings.Default.companyName;
            txtDBpath.Text = DBpath;
        }

        private void button4_Click(object sender, EventArgs e)
        {
            openFileDialog1.ShowDialog();
        }

        private void openFileDialog1_FileOk(object sender, CancelEventArgs e)
        {
            txtDBpath.Text = openFileDialog1.FileName;
        }
    }
}
