﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ppl
{
    public partial class login : Form
    {
        data datos = new data();
        
        public login()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
         {
            try
            {
                if (datos.userExit(txtusr.Text))
                {
                    if (datos.login(txtusr.Text, txtpass.Text))
                    {
                        home ff = new home(txtusr.Text);
                        ff.Show();
                        this.Hide();
                    }
                    else
                    {
                        MessageBox.Show("Info incorrecta");
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void login_Load(object sender, EventArgs e)
        {
            traducir();
            verifyUsers();
        }

        public void verifyUsers()
        {
            if (Convert.ToInt32(datos.getCount("users", "id", string.Empty)) < 1)
            {
                firstTime();
            }
            if (Convert.ToInt32(datos.getCount("users", "id", string.Empty)) == 1)
            {                
                string ff =datos.getSpecificValue("users", "activo", string.Empty);
                if (!(ff=="True"))
                {
                    firstTime();
                }
            }
        }
        public void firstTime()
        {
            usuarios uu = new usuarios();
            uu.Show();
            MessageBox.Show("Por favor crea un usuario para poder acceder.");
        }

        public void traducir()
        {
            string lan = ppl.Properties.Settings.Default.language.ToLower();
            if (lan=="en")
            {
                translate tran = new translate();
                //label2.Text = tran.getWord(label2.Text.ToLower());
                foreach (Control item in this.Controls)
                {
                    string translated = tran.getWord(item.Text.ToLower());
                    item.Text = tran.UppercaseFirst(translated);
                    if (item is GroupBox)
                    {
                        foreach (Control GroupBoxItem in item.Controls)
                        {
                            if (!(GroupBoxItem is PictureBox))
                            {
                                string GroupBoxTranslatedItem = tran.getWord(GroupBoxItem.Text);
                                GroupBoxItem.Text = tran.UppercaseFirst(GroupBoxTranslatedItem);
                            }

                        }
                    }

                }
            }
        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }
    }
}
