﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ppl
{
    public partial class entrada : Form
    {
        data datos = new data();
        public entrada()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            string codigo = "'" +txtcode.Text+ "',";
            string nombre = "'" + txtname.Text + "',";
            string categoria = "'" + txtcategory.Text + "',";
            string fechacompra = "'" + txtdate.Text + "',";
            string preciocompra = "'" + txtbuyprice.Text.Replace(".",",") + "',";
            string precioventa = "'" + txtsaleprice.Text.Replace(".", ",") + "',";
            string precioventamayor = "'" + txtwholesaleprice.Text.Replace(".", ",") + "',";
            string qty = "'" + txtqty.Text + "',";
            string descrip = "'" + txtdescrip.Text + "'";
            if (datos.insert("products", "codigo,nombre,categoria,fechacompra,preciocompra,precioventa,preciomayor,existencia,descripcion", codigo + nombre + categoria + fechacompra + preciocompra + precioventa + precioventamayor + qty + descrip))
            {
                MessageBox.Show("Registrado");
                this.Hide();
            }
            else
            {
                MessageBox.Show("Ups, algo paso... verifica la informacion suministrada o contacta al adinistrador.");
            }
            //datos.insert("codigo",codigo);
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            categorias g = new categorias();
            g.Show();
        }

        private void entrada_Load(object sender, EventArgs e)
        {
            cargar();
            traducir();
        }
        public void traducir()
        {
            string lan = ppl.Properties.Settings.Default.language.ToLower();
            if (lan == "en")
            {
                translate tran = new translate();
                //label2.Text = tran.getWord(label2.Text.ToLower());
                foreach (Control item in this.Controls)
                {
                    string translated = tran.getWord(item.Text.ToLower());
                    item.Text = tran.UppercaseFirst(translated);
                    if (item is GroupBox)
                    {
                        foreach (   Control GroupBoxItem in item.Controls)
                        {
                            if (!(GroupBoxItem is PictureBox) && !(GroupBoxItem is ComboBox) && !(GroupBoxItem is TextBox))
                            {
                                string GroupBoxTranslatedItem = tran.getWord(GroupBoxItem.Text);
                                GroupBoxItem.Text = tran.UppercaseFirst(GroupBoxTranslatedItem);
                            }

                        }
                    }

                }
            }
        }
        public void cargar()
        {
            //var list = datos.getTable("categories", "activa=True").Rows.Cast<DataRow>().ToList();
            var list = datos.getTable("categories", "activa=True");
            txtcategory.ValueMember = "id";
            txtcategory.DisplayMember = "nombre";
            txtcategory.DataSource = list;
        }

        private void linkLabel2_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            cargar();
        }
    }
}
