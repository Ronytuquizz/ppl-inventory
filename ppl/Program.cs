﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ppl
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            //Application.Run(new inventory());
            //Application.Run(new home("test"));
            //Application.Run(new login());
            Application.Run(new Form1());
            //Application.Run(new setting());
            //Application.Run(new salida());
        }
    }
}
